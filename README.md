# ADC Library for General Standards Boards

This library incorporates features of the software developed for MORAY,
SAMS and ICEX-RTR. It can be compiled to support either the 24DSI16WRC
(wide input range) boards or the 24DSI boards.

The appropriate General Standards driver and libraries must already be
installed. This library will only work under Linux.

## Installation

### For 24DSI16WRC Boards

``` shellsession
$ make
$ sudo make install
```

### For 24DSI Boards

``` shellsession
$ make BOARD=24dsi
$ make BOARD=24dsi install
```

## Usage

TODO
