/*
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <gsc_utils.h>
#include <glib.h>
#include "board.h"
#include "gsadc.h"

#define SAMPLE_BYTES(n, chans)      (sizeof(sample_t)*(n)*(chans))
#define DATABLOCK_SIZE(n, chans)    (SAMPLE_BYTES(n, chans) + sizeof(struct timeval) + sizeof(int))

#define DEFAULT_READ_TIMEOUT    10

static void
alloc_queue(ADC_t *adc)
{
    int         i;

    adc->msgq = g_async_queue_new();
    adc->freelist = g_async_queue_new();
    adc->dataq = g_async_queue_new();

    for(i = 0;i < QSIZE;i++)
    {
        adc->block[i] = (adc_data_block_t*)g_slice_alloc(
            DATABLOCK_SIZE(adc->scans_per_block, adc->channels));
        adc->block[i]->scans = adc->scans_per_block;
        adc->block[i]->chans = adc->channels;
        g_async_queue_push(adc->freelist, adc->block[i]);
    }
}

static void
free_queue(ADC_t *adc)
{
    int         i;

    g_async_queue_unref(adc->dataq);
    g_async_queue_unref(adc->freelist);
    g_async_queue_unref(adc->msgq);
    for(i = 0;i < QSIZE;i++)
        g_slice_free1(
            DATABLOCK_SIZE(adc->scans_per_block, adc->channels),
            adc->block[i]);
}

const char*
adc_board_type(void)
{
    return BOARD_TYPE;
}

/**
 * Open the driver device.
 */
ADC_t*
adc_open(const char *dev, unsigned mask, unsigned scans_per_block)
{
    ADC_t       *adc;
    int32_t     arg;
    unsigned    group_0, group_1;

    adc = malloc(sizeof(ADC_t));
    if(!adc)
        g_error("Cannot allocate memory for ADC_t (errno = %d)", errno);

    memset(adc, 0, sizeof(ADC_t));

    if((adc->fd = open(dev, O_RDWR)) < 0)
    {
        g_critical("Cannot open %s (errno = %d)", dev, errno);
        free(adc);
        return NULL;
    }

    query_quiet(adc->fd, QUERY_CHANNELS, &arg);
    adc->total_channels = arg;
    g_debug("A/D channels available: %d", adc->total_channels);
    /* Masks for the two channel groups */
    group_0 = (1 << adc->total_channels/2) - 1;
    group_1 = group_0 << (adc->total_channels/2);

    adc->scans_per_block = scans_per_block;
    adc->mask = mask;
    adc->channels = 0;
    pthread_mutex_init(&adc->sync_mutex, NULL);

    g_debug("A/D channel mask: %04x", adc->mask);

    if(adc->mask & group_0)
        adc->channels += (adc->total_channels/2);
    if(adc->mask & group_1)
        adc->channels += (adc->total_channels/2);

    g_debug("A/D channels to read: %d", adc->channels);

    alloc_queue(adc);

    return adc;
}

unsigned adc_get_nchannels(ADC_t *adc)
{
    return adc->channels;
}

unsigned adc_get_mask(ADC_t *adc)
{
    return adc->mask;
}

void adc_set_udata(ADC_t *adc, void *udata)
{
    adc->udata = udata;
}

/*
 * Compute the clock parameters to produce the requested sampling frequency.
 *
 * @param fd  file device for board driver
 * @param fsamp desired sampling frequency in hz
 * @return actual sampling frequency in hz
 */
static long
compute_clock(int fd, adc_clock_t* cp, long fsamp)
{
#ifdef IOCTL_MASTER_CLK_ADJ
    int32_t     mode;

    /* Bit of a convoluted method for reading FREF */
    cp->fref = QUERY_FREF_DEFAULT;
    ioctl(fd, IOCTL_QUERY, &cp->fref);
    cp->fsamp = fsamp;
    fsamp_validate(fd, &cp->fsamp);
    fsamp_compute(fd, -1, 1, cp->fref,
                  &cp->fsamp,
                  &mode,
                  &cp->nvco,
                  &cp->nref,
                  &cp->ndiv,
                  &cp->mca);
#else
    cp->fref = -1;
    dsi_fref_compute(fd, 1, 0, &cp->fref);
    cp->fsamp = fsamp;
    dsi_fsamp_validate(fd, -1, 1, &cp->fsamp);
    dsi_fsamp_compute(fd, -1, 1, 0, cp->fref,
                      &cp->fsamp,
                      &cp->nvco,
                      &cp->nref,
                      &cp->nrate,
                      &cp->ndiv);
#endif
    return cp->fsamp;
}

static void
_board_set_clock(int fd, unsigned mask, unsigned total_channels, adc_clock_t *cp)
{
    int32_t     src;
    unsigned    group_0, group_1;

    group_0 = (1 << total_channels/2) - 1;
    group_1 = group_0 << (total_channels/2);

#ifdef IOCTL_ADC_MODE
    /* Do we need high-speed mode? ... */
    if(cp->fsamp > 52500)
        ioctl_set(fd, IOCTL_ADC_MODE, ADC_MODE_HI_SPEED);
    else
        ioctl_set(fd, IOCTL_ADC_MODE, ADC_MODE_HI_RES);
#endif

    /* Disable the Aux Clock signal */
#ifdef IOCTL_AUX_CLK_CTL_MODE
    ioctl_set(fd, IOCTL_AUX_CLK_CTL_MODE, AUX_CLK_CTL_MODE_INACTIVE);
#endif
    /*
     * Both channel groups use the internal clock source.
     */
    if(mask & group_0)
        src = CH_GRP_SRC_RATE_GEN;
    else
        src = CH_GRP_SRC_DISABLE;
    ioctl_set(fd, IOCTL_CH_GRP_0_SRC, src);

    if(mask & group_1)
        src = CH_GRP_SRC_RATE_GEN;
    else
        src = CH_GRP_SRC_DISABLE;
    ioctl_set(fd, IOCTL_CH_GRP_1_SRC, src);

#ifdef IOCTL_MASTER_CLK_ADJ
    ioctl(fd, IOCTL_MASTER_CLK_ADJ, &cp->mca);
    ioctl(fd, IOCTL_NVCO, &cp->nvco);
    ioctl(fd, IOCTL_NREF, &cp->nref);
    ioctl(fd, IOCTL_NDIV, &cp->ndiv);
#else
    ioctl(fd, IOCTL_RATE_GEN_A_NVCO, &cp->nvco);
    ioctl(fd, IOCTL_RATE_GEN_B_NVCO, &cp->nvco);
    ioctl(fd, IOCTL_RATE_GEN_A_NREF, &cp->nref);
    ioctl(fd, IOCTL_RATE_GEN_B_NREF, &cp->nref);
    ioctl(fd, IOCTL_RATE_DIV_0_NDIV, &cp->ndiv);
    ioctl(fd, IOCTL_RATE_DIV_1_NDIV, &cp->ndiv);
#endif

}


static void
_board_init(int fd, long bufsize, long timeout, int v_range)
{
    /* Initialize the board hardware */
    ioctl(fd, IOCTL_INITIALIZE, NULL);

    /* DMA mode */
    ioctl_set(fd, IOCTL_RX_IO_MODE, GSC_IO_MODE_DMDMA);
    /* Ignore buffer-overflow to avoid an error on the first read */
    ioctl_set(fd, IOCTL_RX_IO_OVERFLOW, IO_OVERFLOW_IGNORE);
    /* Ignore buffer-underflow on initial read */
    ioctl_set(fd, IOCTL_RX_IO_UNDERFLOW, IO_UNDERFLOW_IGNORE);
    /* Set read timeout (seconds) */
    ioctl_set(fd, IOCTL_RX_IO_TIMEOUT, timeout);
    /* Differential input mode */
    ioctl_set(fd, IOCTL_AI_MODE, AI_MODE_DIFF);
    /* Set voltage range */
    ioctl_set(fd, IOCTL_RANGE, v_range);
#ifdef IOCTL_BURST
    /* Disable burst mode */
    ioctl_set(fd, IOCTL_BURST, BURST_DISABLE);
#endif
#ifdef IOCTL_AI_CHANNEL_TAG
    /* Enable channel-tag for compatibility with non-WRC boards */
    ioctl_set(fd, IOCTL_AI_CHANNEL_TAG, AI_CHANNEL_TAG_ENABLE);
#endif
#ifdef IOCTL_CHANNEL_ORDER
    /* Synchronous channel order */
    ioctl_set(fd, IOCTL_CHANNEL_ORDER, CHANNEL_ORDER_SYNC);
#endif
    /* Enable analog input buffer */
    ioctl_set(fd, IOCTL_AI_BUF_ENABLE, AI_BUF_ENABLE_YES);
    /* Set buffer threshold */
    ioctl_set(fd, IOCTL_AI_BUF_THRESH, bufsize);
    /* Set the sample format */
    ioctl_set(fd, IOCTL_DATA_FORMAT, DATA_FORMAT_2S_COMP);
    /* Set the sample bit width */
    ioctl_set(fd, IOCTL_DATA_WIDTH, DATA_WIDTH_CODE());
    /* Both master and slave are initially set to be initiator */
    ioctl_set(fd, IOCTL_CONTROL_MODE, CONTROL_MODE_INITIATOR);
    /* External clock output */
    ioctl_set(fd, IOCTL_EXT_CLK_SRC, EXT_CLK_SRC_GRP_0);
#ifdef IOCTL_AUX_SYNC_CTL_MODE
    /* AUX SYNC output */
    ioctl_set(fd, IOCTL_AUX_SYNC_CTL_MODE, AUX_SYNC_CTL_MODE_INACTIVE);
#endif
    /* TTL i/o mode */
    ioctl_set(fd, IOCTL_XCVR_TYPE, XCVR_TYPE_TTL);
#ifdef LOW_FREQ_FILTER
    reg_mod(fd, GSC_BCTLR, LOW_FREQ_FILTER, LOW_FREQ_FILTER);
#endif
}

long
adc_init(ADC_t *adc, long fsamp, long timeout, float v_peak)
{
    int32_t     fmax, fmin;
    long        fsamp_calc;
    int         code;

    code = AIN_RANGE_CODE(v_peak, &adc->v_peak);
    _board_init(adc->fd, adc->scans_per_block*adc->channels,
                timeout, code);

    query_quiet(adc->fd, QUERY_FSAMP_MIN, &fmin);
    query_quiet(adc->fd, QUERY_FSAMP_MAX, &fmax);

    fsamp_calc = compute_clock(adc->fd, &adc->clock, fsamp);
    if(fsamp_calc < fmin || fsamp_calc > fmax)
    {
        g_critical("Cannot set sampling frequency (%ld)",
                   fsamp_calc);
        return 0;
    }

    _board_set_clock(adc->fd, adc->mask, adc->total_channels, &adc->clock);
#ifdef IOCTL_AUTO_CALIBRATE
    g_message("Autocalibrating ...");
    ioctl(adc->fd, IOCTL_AUTO_CALIBRATE, NULL);
    g_message("Calibration complete");
#endif
    g_debug("Clearing buffers");

    ioctl(adc->fd, IOCTL_AI_BUF_CLEAR, NULL);
    if(adc_sync(adc) < 0)
        g_critical("Sync failed");

    return fsamp_calc;
}

void
adc_input_mode(ADC_t *adc, adc_input_mode_t mode)
{
    int32_t   mode_val = AI_MODE_DIFF;

    switch(mode)
    {
        case INPUT_DIFF:
            mode_val = AI_MODE_DIFF;
            break;
        case INPUT_ZERO:
            mode_val = AI_MODE_ZERO;
            break;
        case INPUT_VREF:
            mode_val = AI_MODE_VREF;
            break;
    }

    ioctl_set(adc->fd, IOCTL_AI_MODE, mode_val);
    /* Allow at least 100ms of settling time */
    usleep(150000);
}

void
adc_vrange(ADC_t *adc, float v_peak)
{
    int     code;

    code = AIN_RANGE_CODE(v_peak, &adc->v_peak);
    ioctl_set(adc->fd, IOCTL_RANGE, code);
}

void
adc_enable(ADC_t *adc)
{
    ioctl_set(adc->fd, IOCTL_AI_BUF_ENABLE,
              AI_BUF_ENABLE_YES);
}

void
adc_disable(ADC_t *adc)
{
    ioctl_set(adc->fd, IOCTL_AI_BUF_ENABLE,
              AI_BUF_ENABLE_NO);
}

void
adc_close(ADC_t *adc)
{
    g_return_if_fail(adc);

    pthread_mutex_destroy(&adc->sync_mutex);
    free_queue(adc);
    close(adc->fd);
    free(adc);
}

void
adc_debug_regs(ADC_t *adc, int detail)
{
    reg_list(adc->fd, detail);
}
