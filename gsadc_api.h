/*
** Public API for the MORAY ADC library.
*/
#ifndef _GSADC_API_H_
#define _GSADC_API_H_

#ifndef GSADC_NOINCLUDE
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#endif

#ifndef GSADC_API
#   if __GNUC__ >= 4
#       define GSADC_API __attribute__((visibility("default")))
#   else
#       define GSADC_API
#   endif
#endif

/*
 * We are configuring the board for 24-bit 2's complement data values. The
 * following macros are used to extract the data value from the sample word.
 */
#define GSADC_DATA_SIZE               24
#define GSADC_DATA_MASK               ((1UL << GSADC_DATA_SIZE) - 1)
#define GSADC_DATA_SIGN_BIT           (1 << (GSADC_DATA_SIZE-1))

typedef uint32_t adc_sample_t;

static __inline__ int32_t GSADC_DATA_VALUE(adc_sample_t sample)
{
    return (sample & GSADC_DATA_SIGN_BIT) ?
      (sample | ~GSADC_DATA_MASK) :
      (sample & GSADC_DATA_MASK);
}

typedef struct {
    struct timespec     ts;         /**< block timestamp */
    int                 scans;      /**< number of scans (rows) */
    int                 chans;      /**< number of channels (cols) */
    adc_sample_t        data[1];
} adc_data_block_t;

struct adc_clock_s;
typedef struct adc_clock_s adc_clock_t;

struct ADC_s;
typedef struct ADC_s ADC_t;

typedef enum {
    TRIG_START=0,
    TRIG_END=1
} adc_trig_state_t;

typedef enum {
    INPUT_DIFF=0,
    INPUT_ZERO=1,
    INPUT_VREF=2
} adc_input_mode_t;


typedef void (*DataFunc)(adc_data_block_t *dp, unsigned mask, void *udata);
typedef int (*StreamFunc)(adc_data_block_t *dp, unsigned mask, void *udata);
typedef void (*TrigFunc)(adc_trig_state_t state, char *token, void *udata);

GSADC_API const char* adc_board_type(void);
GSADC_API ADC_t* adc_open(const char *dev, unsigned mask, unsigned scans_per_block);
GSADC_API unsigned adc_get_nchannels(ADC_t *adc);
GSADC_API unsigned adc_get_mask(ADC_t *adc);
GSADC_API void adc_set_udata(ADC_t *adc, void *udata);
GSADC_API int adc_channels_ready(ADC_t *adc, long timeout_ms);
GSADC_API int adc_sync(ADC_t *adc);

GSADC_API long adc_init(ADC_t *adc, long fsamp, long timeout, float v_peak);
GSADC_API void adc_enable(ADC_t *adc);
GSADC_API void adc_disable(ADC_t *adc);
GSADC_API void adc_close(ADC_t *adc);
GSADC_API int adc_arm(ADC_t *adc, long timeout_ms, int use_hardware);
GSADC_API int adc_trigger(ADC_t *adc);
GSADC_API long adc_read(ADC_t *adc, long n_samples, DataFunc f, TrigFunc tf);
GSADC_API void adc_vrange(ADC_t *adc, float v_peak);
GSADC_API void adc_input_mode(ADC_t *adc, adc_input_mode_t mode);
GSADC_API unsigned long adc_stream(ADC_t *adc, StreamFunc f, TrigFunc tf);
GSADC_API void adc_debug_regs(ADC_t *adc, int detail);
#endif /* _GSADC_API_H_ */
