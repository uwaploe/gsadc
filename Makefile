#
# Makefile for GS ADC library.
#

# 24dsi16wrc or 24dsi
BOARD = 24dsi16wrc

ifeq ($(BOARD),24dsi16wrc)
VAR =
DEFS = -D__WRC__
else
VAR = -nowrc
DEFS =
endif

CC = gcc
CPPFLAGS = $(DEFS) -I. $(shell pkg-config --cflags glib-2.0 uuid gs$(BOARD))
CFLAGS = -Wall -O2


INSTALL = install
LIBTOOL = libtool

prefix = /usr
LIBDIR = $(DESTDIR)$(prefix)/lib
BINDIR = $(DESTDIR)$(prefix)/bin
INCDIR = $(DESTDIR)$(prefix)/include
PCDIR = $(DESTDIR)$(prefix)/share/pkgconfig

VERS := "1.4.4"
# See section 7.3 of the Libtool manual for an explanation of the
# library version format.
LIB_VERS := "4:3:2"

PROG := testlib
lib_LTLIBRARIES := libgsadc$(VAR).la
lib_la_SOURCES := $(wildcard adc_*.c)
lib_la_OBJECTS := $(lib_la_SOURCES:.c=.lo)
lib_la_DEPLIBS := $(shell pkg-config --libs glib-2.0 uuid) \
	-l$(BOARD)_utils -l$(BOARD)_dsl -lgsc_utils -lrt
lib_la_LDFLAGS := -version-info $(LIB_VERS) -rpath $(prefix)/lib \
	$(lib_la_DEPLIBS)

HEADERS := gsadc_api.h
PKGCONFIG := gsadc.pc

all: $(lib_LTLIBRARIES)

prog: $(PROG)

$(lib_la_OBJECTS): %.lo: %.c
	$(LIBTOOL) --mode=compile $(CC) $(CPPFLAGS) $(CFLAGS)  -c $<

$(lib_LTLIBRARIES): $(lib_la_OBJECTS)
	$(LIBTOOL) --mode=link $(CC) -o $@ $^ $(lib_la_LDFLAGS)

testlib.o: testlib.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

testlib: testlib.o $(lib_LTLIBRARIES)
	$(LIBTOOL) --mode=link $(CC) -o $@ $^

$(PKGCONFIG): $(PKGCONFIG).in
	sed -e "s/@VERSION@/$(VERS)/g" \
	    -e "s!@PREFIX@!$(prefix)!g" \
        -e "s!@REQ@!gs$(BOARD)!g" \
        -e "s!@VAR@!$(VAR)!g" \
	  $(PKGCONFIG).in > $(PKGCONFIG)

install-lib: $(lib_LTLIBRARIES) $(PKGCONFIG)
	$(INSTALL) -d $(LIBDIR) $(INCDIR) $(PCDIR)
	$(LIBTOOL) --mode=install $(INSTALL) $(lib_LTLIBRARIES) $(LIBDIR)
	$(INSTALL) $(HEADERS) $(INCDIR)
	$(INSTALL) $(PKGCONFIG) $(PCDIR)

install-bin: $(lib_LTLIBRARIES) $(PKGCONFIG) $(PROG)
	$(INSTALL) -d $(BINDIR)
	$(LIBTOOL) --mode=install $(INSTALL) -c $(PROG) $(BINDIR)/$(PROG)

install: install-lib

clean:
	rm -f *.o *.so *~ *.la *.lo $(PKGCONFIG) testlib
	rm -rf .libs
