#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include "gsadc_api.h"


static void show_scan(adc_data_block_t *dp, unsigned cmask, void *udata)
{
    int             i, j;
    unsigned        mask;
    adc_sample_t    *m_buf;

    assert(udata == (void*)0);

    m_buf = dp->data;

    for(j = 0; j < dp->scans; j++)
    {
        for(i = 0,mask = 1; i < dp->chans; i++,m_buf++,mask <<= 1)
        {
            if((mask & cmask) == mask)
                printf("%d ", GSADC_DATA_VALUE(*m_buf));

        }
        printf("\n");
    }
}

int main(int ac, char **av)
{
    ADC_t   *adc;
    int     t_wait = 10;
    char    devname[64];

    if(ac > 1)
        t_wait = atoi(av[1]);

    snprintf(devname, sizeof(devname)-1, "/dev/%s.0", adc_board_type());

    adc = adc_open(devname, 0xff, 10);
    if(adc != NULL)
    {
        adc_init(adc, 20000, 10, 10.);
        adc_set_udata(adc, 0);
        printf("Reading zero reference (external trigger)\n");
        adc_input_mode(adc, INPUT_ZERO);
        adc_arm(adc, t_wait*1000, 1);
        adc_debug_regs(adc, 0);
        printf("Waiting %d seconds for external trigger ...\n", t_wait);
        adc_read(adc, 10, show_scan, NULL);

        printf("Reading differential input (internal trigger)\n");
        adc_input_mode(adc, INPUT_DIFF);
        adc_arm(adc, t_wait*1000, 0);
        adc_debug_regs(adc, 0);
        if(adc_trigger(adc) < 0)
        {
            sleep(2);
            adc_trigger(adc);
        }
        adc_read(adc, 10, show_scan, NULL);
        adc_close(adc);
    }
    else
        return 1;

    return 0;
}
