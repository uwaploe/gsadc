/*
**
*/
#ifndef _GSADC_H_
#define _GSADC_H_

#include "gsadc_api.h"

#include <sys/ioctl.h>
#include <errno.h>
#include <glib.h>
#include <semaphore.h>
#include <signal.h>
#include <pthread.h>


#define sample_t        adc_sample_t
#define data_block_t    adc_data_block_t

/* Mask to extract the channel tag from each sample */
#define CHANNEL_TAG_MASK        0x1f000000

#define DATA_SIZE               GSADC_DATA_SIZE
#define DATA_MASK               GSADC_DATA_MASK
#define DATA_SIGN_BIT           GSADC_DATA_SIGN_BIT
#define DATA_VALUE              GSADC_DATA_VALUE

static __inline__ int DATA_WIDTH_CODE(void)
{
    switch(DATA_SIZE)
    {
        case 24:
            return DATA_WIDTH_24;
        case 20:
            return DATA_WIDTH_20;
        case 18:
            return DATA_WIDTH_18;
        default:
            return DATA_WIDTH_16;
    }
}

static __inline__ int AIN_RANGE_CODE(float v_want, float *v_got)
{
    float v = (v_want < 0.) ? -v_want : v_want, vg;
    int   code;

    vg = 5.;
    code = RANGE_5V;

    if(v > 5.)
    {
        vg = 10.;
        code = RANGE_10V;
    }
    else if(v > 2.5)
    {
        vg = 5.;
        code = RANGE_5V;
    }
    else if(v > 1.25)
    {
        vg = 2.5;
        code = RANGE_2_5V;
    }
#ifdef RANGE_1_25V
    else if(v > 1.)
    {
        vg = 1.25;
        code = RANGE_1_25V;
    }
#endif
#ifdef RANGE_1V
    else if(v > 0.1)
    {
        vg = 1.;
        code = RANGE_1V;
    }
#endif
#ifdef RANGE_100MV
    else if(v > 0.01)
    {
        vg = 0.1;
        code = RANGE_100MV;
    }
#endif
#ifdef RANGE_10MV
    else
    {
        vg = 0.01;
        code = RANGE_10MV;
    }
#endif
    if(v_got)
        *v_got = vg;
    return code;
}


struct adc_clock_s {
    int32_t fref;   /**> Reference oscillator frequency */
    int32_t fsamp;  /**> Sample frequency */
    int32_t nvco;   /**> PLL VCO factor */
    int32_t nref;   /**> PLL reference factor */
    int32_t ndiv;   /**> Rate divisor */
#ifdef IOCTL_MASTER_CLK_ADJ
    int32_t mca;    /**> Master Clock Adjust value */
#else
    int32_t nrate;
#endif
};

#define SCAN_BUF_LEN    20
#define QSIZE           10

struct ADC_s {
    int                 fd;                 /**> File descriptor */
    unsigned            mask;               /**> Active channels */
    unsigned            channels;           /**> Channels sampled */
    unsigned            total_channels;     /**> Channels on the board */
    adc_clock_t         clock;              /**> Clock settings */
    sem_t               triggered;          /**> Semaphore to signal a TRIGGER event */
    long                trigger_timeout;
    unsigned            scans_per_block;
    float               v_peak;
    void                *udata;
    pthread_t           t_trigger;
    pthread_mutex_t     sync_mutex;
    GAsyncQueue         *msgq;
    GAsyncQueue         *freelist;
    GAsyncQueue         *dataq;
    adc_data_block_t    *block[QSIZE];
};

static __inline__ int _ioctl_set(int fd, int req, int val, const char *reqname)
{
    int32_t _val = val;
    int r;

    g_debug("ioctl(%d, %s)", fd, reqname);
    r = ioctl(fd, req, &_val);
    if(r < 0)
        g_warning("ioctl(%s) failed, errno = %d", reqname, errno);
    return r;
}

#define ioctl_set(fd, req, val) _ioctl_set(fd, req, val, #req)


#endif /* _MORAY_ADC_H_ */
