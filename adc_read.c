/*
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <gsc_utils.h>
#include <glib.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <uuid/uuid.h>
#include "board.h"
#include "gsadc.h"

static int intr_signals[] = {SIGHUP, SIGINT, SIGQUIT, SIGTERM};
static sig_atomic_t stop_stream = 0;
typedef unsigned long msg_t;

#define NR_INTS (sizeof(intr_signals)/sizeof(int))
#ifndef MIN
#define MIN(a, b)   ((a) < (b) ? (a) : (b))
#endif

/*
 * Sign extend the data values to 32-bit integers
 */
static void
sign_extend(adc_sample_t *data, int n)
{
    int         i;
    for(i = 0;i < n;i++)
        data[i] = GSADC_DATA_VALUE(data[i]);
}

/*
 * Get a block from the free-list. This function does not block,
 * it returns NULL if the free-list is empty.
 */
static data_block_t*
acquire_block(ADC_t *adc)
{
    adc_data_block_t        *dp;

    dp = (adc_data_block_t*)g_async_queue_try_pop(adc->freelist);
    if(dp == NULL)
        g_critical("Data queue is full!");
    return dp;
}

/*
 * Return a block to the free-list.
 */
static void
release_block(ADC_t *adc, adc_data_block_t *dp)
{
    /*
     * dp->scans can be changed by adc_read. Set it back to the
     * default value before returning it to the free-list.
     */
    dp->scans = adc->scans_per_block;
    g_async_queue_push(adc->freelist, dp);
}

/*
 * Return all blocks to the free-list.
 */
static void
release_all(ADC_t *adc)
{
    adc_data_block_t        *dp;
    int                     count = 0;

    while((dp = (adc_data_block_t*)g_async_queue_try_pop(adc->dataq)) != NULL)
    {
        count++;
        release_block(adc, dp);
    }
    g_debug("Released %d blocks", count);
}

/*
 * Enable buffer overflow check
 */
static void
enable_buffer_check(ADC_t* adc)
{
    int32_t arg;

    arg = AI_BUF_OVERFLOW_CLEAR;
    ioctl(adc->fd, IOCTL_AI_BUF_OVERFLOW, &arg);

    arg = IO_OVERFLOW_CHECK;
    ioctl(adc->fd, IOCTL_RX_IO_OVERFLOW, &arg);
}

static int
wait_for_ready(ADC_t *adc)
{
#ifdef IOCTL_BURST
    /*
     * In burst-mode, the read should block until triggered.
     */
    adc_data_block_t    *dp;
    size_t              blocksize, n_read;

    blocksize = adc->scans_per_block * adc->channels * sizeof(adc_sample_t);
    dp = acquire_block(adc);
    g_debug("Waiting for TRIGGER event");
    clock_gettime(CLOCK_MONOTONIC_RAW, &dp->ts);
    n_read = read(adc->fd, dp->data, blocksize);
    g_async_queue_push(adc->dataq, dp);
    return (n_read == blocksize);
#else
    /*
     * In non-burst-mode, we need to wait for a CHANNELS_READY event.
     */
    gsc_wait_t  wait;
    int32_t     save_irq, irq;
    int32_t     sync_mode;

    /* Save current IRQ setting */
    save_irq = -1;
    ioctl(adc->fd, IOCTL_IRQ_SEL, &save_irq);
    irq = IRQ_CHAN_READY;
    ioctl(adc->fd, IOCTL_IRQ_SEL, &irq);

    g_debug("Waiting for TRIGGER event");

    memset(&wait, 0, sizeof(wait));
    wait.gsc = WAIT_GSC_CHAN_READY;
    wait.timeout_ms = adc->trigger_timeout;
    ioctl(adc->fd, IOCTL_WAIT_EVENT, &wait);

    /* Restore IRQ setting */
    ioctl(adc->fd, IOCTL_IRQ_SEL, &save_irq);
    /* Restore normal SYNC mode */
    sync_mode = SW_SYNC_MODE_SYNC;
    ioctl(adc->fd, IOCTL_SW_SYNC_MODE, &sync_mode);

    return (wait.flags == GSC_WAIT_FLAG_DONE);
#endif
}

/*
 * Thread to wait for next SYNC and then start reading data from
 * the A/D board and appending it to the data-block queue.
 *
 */
static void*
data_reader(void *thread_arg)
{
    ADC_t               *adc = (ADC_t*)thread_arg;
    sigset_t            mask;
    int                 i;
    size_t              blocksize, n_read;
    adc_data_block_t    *dp;

    pthread_detach(pthread_self());

    /*
     * Insure that any interrupting signals are delivered to the main thread.
     */
    sigemptyset(&mask);
    for(i = 0;i < NR_INTS;i++)
        sigaddset(&mask, intr_signals[i]);

    pthread_sigmask(SIG_BLOCK, &mask, NULL);

    blocksize = adc->scans_per_block * adc->channels * sizeof(adc_sample_t);
    if(wait_for_ready(adc))
    {
        sem_post(&adc->triggered);
        enable_buffer_check(adc);
        while(g_async_queue_try_pop(adc->msgq) == NULL)
        {
            dp = acquire_block(adc);
            if(dp)
            {
                clock_gettime(CLOCK_MONOTONIC_RAW, &dp->ts);
                n_read = read(adc->fd, dp->data, blocksize);
                if(n_read < 0)
                {
                    g_critical("Error reading ADC: %s", g_strerror(errno));
                    break;
                }
                else if(n_read < blocksize)
                    g_critical("Short read: %ld bytes", n_read);
                g_async_queue_push(adc->dataq, dp);
    release_all(adc);
            }
            else
                break;
        }
    }
    else
        g_critical("TRIGGER not detected");

    g_debug("Thread complete");

    return NULL;
}

int
adc_arm(ADC_t *adc, long timeout_ms, gboolean use_trigger)
{
    int                 err;
    adc_data_block_t    *dp;

    sem_init(&adc->triggered, 0, 0);
    adc->trigger_timeout = timeout_ms;
    ioctl_set(adc->fd, IOCTL_RX_IO_TIMEOUT, (int32_t)(timeout_ms/1000));

    /*
     * Empty the data-block queue
     */
    do
    {
        dp = (adc_data_block_t*)g_async_queue_try_pop(adc->dataq);
        if(dp)
            release_block(adc, dp);
    } while(dp != NULL);

#ifdef IOCTL_BURST
    adc_disable(adc);
    ioctl_set(adc->fd, IOCTL_AI_BUF_CLEAR, 0);
    ioctl_set(adc->fd, IOCTL_BURST_TIMER, BURST_TIMER_DISABLE);
    ioctl_set(adc->fd, IOCTL_BURST_SIZE, 0);
    ioctl_set(adc->fd, IOCTL_BURST, BURST_ENABLE);
    adc_enable(adc);
#else
    /* Ensure that the board is in Initiator mode. */
    ioctl_set(adc->fd, IOCTL_CONTROL_MODE, CONTROL_MODE_INITIATOR);
    /* Next SYNC will clear the buffer */
    ioctl_set(adc->fd, IOCTL_SW_SYNC_MODE, SW_SYNC_MODE_CLR_BUF);
    if(use_trigger)
    {
        err = reg_mod(adc->fd, GSC_BCTLR, ARM_EXTERNAL_TRIGGER,
                      ARM_EXTERNAL_TRIGGER);
        if(err > 0)
            g_critical("Register write error, errno = %d", errno);
    }
#endif

    /* Start the reader thread */
    err = pthread_create(&adc->t_trigger, NULL, data_reader, adc);

    if(err != 0)
    {
        g_critical("Cannot create thread (error code = %d)", err);
        sem_destroy(&adc->triggered);
        adc->trigger_timeout = -1;
        return -1;
    }

    return 0;
}

int
adc_trigger(ADC_t *adc)
{
#ifdef IOCTL_BURST
    ioctl(adc->fd, IOCTL_BURST_TRIGGER, NULL);
    return 0;
#else
    if(pthread_mutex_trylock(&adc->sync_mutex) == 0)
    {
        g_debug("Sending software SYNC");
        ioctl(adc->fd, IOCTL_SW_SYNC, NULL);
        pthread_mutex_unlock(&adc->sync_mutex);
        g_debug("SYNC sent");
        return 0;
    }
    else
        return -1;
#endif
}

long
adc_read(ADC_t *adc, long n_scans, DataFunc handler, TrigFunc tf)
{
    long                n;
    adc_data_block_t    *dp;
    struct timespec     ts;
    msg_t               *pmsg;
    uuid_t              uuid;
    char                uuid_buf[40];

    if(adc->trigger_timeout <= 0)
        return -1;

    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += adc->trigger_timeout/1000;
    ts.tv_nsec += ((adc->trigger_timeout % 1000)*1000000);

    n = n_scans;
    uuid_generate_time(uuid);
    uuid_unparse(uuid, uuid_buf);

    pmsg = g_malloc0(sizeof(msg_t));
    if(sem_timedwait(&adc->triggered, &ts) == 0)
    {
        if(tf)
            tf(TRIG_START, uuid_buf, adc->udata);

        /* We don't want adc_trigger called during a read */
        pthread_mutex_lock(&adc->sync_mutex);
        g_debug("Reading data");
        while(n > 0)
        {
            dp = (adc_data_block_t*)g_async_queue_pop(adc->dataq);
            dp->scans = MIN(n, dp->scans);
            sign_extend(dp->data, dp->scans * dp->chans);
            if(handler)
                handler(dp, adc->mask, adc->udata);
            n -= dp->scans;
            release_block(adc, dp);
        }
        g_async_queue_push(adc->msgq, pmsg);
        pthread_mutex_unlock(&adc->sync_mutex);
#ifdef IOCTL_BURST
        ioctl_set(adc->fd, IOCTL_BURST, BURST_DISABLE);
#endif
        if(tf)
            tf(TRIG_END, NULL, adc->udata);
    }
    else
        g_critical("Timeout waiting for trigger");

    sem_destroy(&adc->triggered);
    g_free(pmsg);

    /*
     * Sampling is complete, we can return to ignoring buffer
     * overflows.
     */
    ioctl_set(adc->fd, IOCTL_RX_IO_OVERFLOW,
              IO_OVERFLOW_IGNORE);
    ioctl_set(adc->fd, IOCTL_AI_BUF_OVERFLOW,
              AI_BUF_OVERFLOW_CLEAR);

    return (n_scans - n);
}

static void
catch_signal(int signum)
{
    stop_stream = 1;
}

unsigned long
adc_stream(ADC_t *adc, StreamFunc handler, TrigFunc tf)
{
    unsigned long       n;
    int                 i;
    adc_data_block_t    *dp;
    struct timespec     ts;
    msg_t               *pmsg;
    struct sigaction    sa, old_sa[NR_INTS];
    uuid_t              uuid;
    char                uuid_buf[40];

    if(adc->trigger_timeout <= 0)
        return 0;

    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += adc->trigger_timeout/1000;
    ts.tv_nsec += ((adc->trigger_timeout % 1000)*1000000);

    n = 0;
    uuid_generate_time(uuid);
    uuid_unparse(uuid, uuid_buf);

    /*
     * Install a handler for the signals which can interrupt the stream.
     */
    stop_stream = 0;
    sa.sa_handler = catch_signal;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    for(i = 0;i < NR_INTS;i++)
        sigaction(intr_signals[i], &sa, &old_sa[i]);

    pmsg = g_malloc0(sizeof(msg_t));
    if(sem_timedwait(&adc->triggered, &ts) == 0)
    {
        if(tf)
            tf(TRIG_START, uuid_buf, adc->udata);

        /* We don't want adc_trigger called during a read */
        pthread_mutex_lock(&adc->sync_mutex);
        g_debug("Reading data");
        while(!stop_stream)
        {
            dp = (adc_data_block_t*)g_async_queue_pop(adc->dataq);
            sign_extend(dp->data, dp->scans * dp->chans);
            stop_stream = handler(dp, adc->mask, adc->udata);
            n += dp->scans;
            release_block(adc, dp);
        }
        g_async_queue_push(adc->msgq, pmsg);
        pthread_mutex_unlock(&adc->sync_mutex);
#ifdef IOCTL_BURST
        ioctl_set(adc->fd, IOCTL_BURST, BURST_DISABLE);
#endif
        if(tf)
            tf(TRIG_END, NULL, adc->udata);
    }
    else
        g_critical("Timeout waiting for trigger");

    /* Restore original signal handlers */
    for(i = 0;i < NR_INTS;i++)
        sigaction(intr_signals[i], &old_sa[i], NULL);

    sem_destroy(&adc->triggered);
    g_free(pmsg);

    /*
     * Sampling is complete, we can return to ignoring buffer
     * overflows.
     */
    ioctl_set(adc->fd, IOCTL_RX_IO_OVERFLOW,
              IO_OVERFLOW_IGNORE);
    ioctl_set(adc->fd, IOCTL_AI_BUF_OVERFLOW,
              AI_BUF_OVERFLOW_CLEAR);

    return n;
}
